import fs from 'fs';
import path from 'path';

// Simple error handler for files, we're just going to try our best and not error out
const fileWriteErrHandler = err => {
  if (err) {
    console.error('Slow Query logger unable to write file', err);
  }
};

const jsonTransport = ({ folder, format }) => {
  return (output) => {
    if (folder) {
      const prefix = `${output.requestedAt.getTime()}.${Math.floor(Math.random() * 1000 + 1)}.${output.operation}`;

      if (format) {
        fs.writeFile(path.join(folder, prefix + '.json'), JSON.stringify(output, null, 2), fileWriteErrHandler);
      } else {
        fs.writeFile(path.join(folder, prefix + '.json'), JSON.stringify(output), fileWriteErrHandler);
      }
    }
  };
};

export default jsonTransport;
