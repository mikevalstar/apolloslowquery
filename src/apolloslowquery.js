function endTimer (time, precision) {
  function roundTo (decimalPlaces, numberToRound) {
    const factorOfTen = Math.pow(10, decimalPlaces);

    return +(Math.round(numberToRound * factorOfTen) / factorOfTen);
  }
  const diff = process.hrtime.bigint() - time;
  const elapsed = Number(diff) / 1000000; // convert from bigint to regular number, it'll be small enough

  return roundTo(precision || 4, elapsed); // Result in milliseconds to 4 decimal places
}

const apolloSlowQuery = ({ slow, precision, transport, ignoreOperations }) => {
  // make into an array so teh includes will always work
  const ignore = Array.isArray(ignoreOperations) ? ignoreOperations : [];

  return {

    // Inform the user they are doing somethign dangerous
    async serverWillStart () {
      console.warn('🦥 Slow query log plugin enabled! do not use in production');
    },

    async requestDidStart () {
      const queryDate = new Date();
      const reqStart = process.hrtime.bigint();
      const trace = [];
      const errors = [];

      let timeUntilExecutionMs = null;

      return {

        async executionDidStart () {
          timeUntilExecutionMs = endTimer(reqStart, precision);

          return {
            willResolveField ({ info }) {
              if (!info || !info.path) {
                return; // just being safe
              }

              const fieldStart = endTimer(reqStart, precision);
              const fieldTimer = process.hrtime.bigint();

              let p = info.path;

              let res = `${p.key}`;

              while ((p = p.prev) !== undefined) {
                res = `${p.key}.${res}`;
              }

              return () => {
                trace.push({
                  path: res,
                  start: fieldStart,
                  duration: endTimer(fieldTimer, precision),
                });
              };
            },
          };
        },

        async didEncounterErrors ({ errors }) {
          errors.push(errors);
        },

        async willSendResponse (sendContext) {
          const durationMs = endTimer(reqStart, precision);

          if (!ignore.includes(sendContext.operationName) && durationMs > slow) {
            const query = sendContext.source; // use instead of requestContext.request.query in case the client is hashing queries
            const reqOp = sendContext.operationName || 'Unknown';
            const variables = sendContext.request.variables;
            const resp = JSON.stringify(sendContext.response.data, null, 2);

            const output = {
              durationMs,
              timeUntilExecutionMs,
              requestedAt: queryDate,
              operation: reqOp,
              query,
              variables,
              response: resp,
              trace,
              errors,
            };

            console.info(`🦥 Slow Query: ${reqOp} - ${durationMs} ms`);

            if (transport) {
              transport(output);
            } else {
              console.info(output);
            }
          }
        },
      };
    },

  };
};

export default apolloSlowQuery;
