import { build } from 'vite';
import path from 'path';
import * as url from 'url';
// const __filename = url.fileURLToPath(import.meta.url);
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));

const components = [
  {
    entry: path.resolve(__dirname, 'src/apolloslowquery.js'),
    name: 'apolloslowquery',
    filename: 'apolloslowquery',
  },
  {
    entry: path.resolve(__dirname, 'src/yaml.js'),
    name: 'yaml',
    filename: 'yaml',
  },
  {
    entry: path.resolve(__dirname, 'src/json.js'),
    name: 'json',
    filename: 'json',
  },
];

components.forEach(async item => {
  await build({
    configFile: false,
    build: {
      emptyOutDir: false,
      sourcemap: true,
      minify: 'esbuild',
      lib: {
        entry: item.entry,
        name: item.name,
        filename: item.filename,
      },
      rollupOptions: {
        external: ['path', 'fs'],
        output: {
          entryFileNames: `${item.filename}.[format].js`,
        },
      },
    },
  });
});
