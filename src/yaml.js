import fs from 'fs';
import path from 'path';
import { stringify } from 'yaml';

// Simple error handler for files, we're just going to try our best and not error out
const fileWriteErrHandler = err => {
  if (err) {
    console.error('Slow Query logger unable to write file', err);
  }
};

const yamlTransport = ({ folder }) => {
  return (output) => {
    if (folder) {
      const prefix = `${output.requestedAt.getTime()}.${Math.floor(Math.random() * 1000 + 1)}.${output.operation}`;

      fs.writeFile(path.join(folder, prefix + '.yaml'), stringify(output), fileWriteErrHandler);
    }
  };
};

export default yamlTransport;
