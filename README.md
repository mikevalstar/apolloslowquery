# Apollo Slow Query

A small (2kb) script for logging out slow queries in Apollo GraphQL.

this is suitable for development purposes to help you find and debug slow queries with Apollo server.

## [Repo Website](https://gitlab.com/mikevalstar/apolloslowquery)

## Usage
To use it `yarn add @mikevalstar/apolloslowquery`

```javascript
import apolloSlowQuery from '@mikevalstar/apolloslowquery';
import apolloSlowQueryYaml from '@mikevalstar/apolloslowquery/yaml';

const aServer = new ApolloServer({
  schema: schemaWithResolvers,
  plugins: [
    apolloSlowQuery({slow: 20, transport: apolloSlowQueryYaml({folder: path.join(__dirname, 'slow')}), ignoreOperations: ['IntrospectionQuery']}),
  ],
});
```

## Libraries
this project will output to the console by default but we do include the YAML package for use with the yaml transport if you choose to use it

## Options

`slow` - **required** the threshold in ms that a query is considered slow

`transport` - a custom transport for teh output data, the default is to log the json view to the console

`ignoreOperations` - an array of operation names to ignore for specific operations you don't want to log

## Sample Output

```yaml
durationMs: 27.3621
timeUntilExecutionMs: 4.3021
requestedAt: 2022-09-17T15:32:01.250Z
operation: ExampleQueryNew
query: |
  query ExampleQueryNew($blogId: Int!, $userId: Int!) {
    roles {
      display
      notes
    }
    blog(blogId: $blogId) {
      publishedAt
      id
    }
    user(userId: $userId) {
      roles {
        display
        notes
      }
    }
  }
variables:
  blogId: 1
  userId: 1
response: |-
  {
    "roles": [
      {
        "display": "Owner",
        "notes": "Full rights to the plication"
      },
      {
        "display": "Admin",
        "notes": "Can modify global settings"
      },
      {
        "display": "User Admin",
        "notes": "Can manage users"
      },
      {
        "display": "Movie Reviewer",
        "notes": "Can manage movies"
      },
      {
        "display": "Form Builder",
        "notes": "Can create forms"
      }
    ],
    "blog": {
      "publishedAt": null,
      "id": 1
    },
    "user": {
      "roles": [
        {
          "display": "Owner",
          "notes": "Full rights to the plication"
        }
      ]
    }
  }
trace:
  - path: roles
    start: 5.5969
    duration: 2.3473
  - path: roles.0.display
    start: 8.1237
    duration: 0.0466
  - path: roles.0.notes
    start: 8.2163
    duration: 0.0077
  - path: roles.1.display
    start: 8.3109
    duration: 0.0084
  - path: roles.1.notes
    start: 8.3455
    duration: 0.0093
  - path: roles.2.display
    start: 8.3959
    duration: 0.103
  - path: roles.2.notes
    start: 8.5371
    duration: 0.0108
  - path: roles.3.display
    start: 8.5767
    duration: 0.0068
  - path: roles.3.notes
    start: 8.6066
    duration: 0.0093
  - path: roles.4.display
    start: 8.65
    duration: 0.0075
  - path: roles.4.notes
    start: 8.7569
    duration: 0.0082
  - path: user
    start: 7.7654
    duration: 7.2526
  - path: user.roles
    start: 15.1661
    duration: 0.2243
  - path: user.roles.0.display
    start: 15.4911
    duration: 0.0149
  - path: user.roles.0.notes
    start: 15.5373
    duration: 0.004
  - path: blog
    start: 6.2749
    duration: 20.7278
  - path: blog.publishedAt
    start: 27.0924
    duration: 0.0063
  - path: blog.id
    start: 27.1188
    duration: 0.006
errors: []

```

## Custom Transports

By default the system will output to the console, you can use the built in `yaml`, or `json` transports as in the example usage above.

If you wish to write your own transport you can pass in a function:

```javascript
const exampleTransport = (options) => {
  return (output) => {
    console.log(output);
    /*
    format: 
    {
      durationMs, // Overall time taken
      timeUntilExecutionMs, // wait time before execution
      requestedAt, // Date() of the request
      operation, // Operation name
      query, // The raw query
      variables, // The raw variables for the query
      response, // The response from apollo
      trace, // per resolver timings
      errors, // output of any errors from the query
    }
    */
  };
};

export default exampleTransport;
```